/*
  Represents object of pawn. Contains it's model reference.
*/

function Pawn(type, x, y, z, pawnModelMaterial) {
  this.type = type; //green / black
  this.x = x;
  this.y = y;
  this.z = z;

  var pawnMesh;

  this.loadModel = (url, callback) => {
    //loader
    loader = new THREE.OBJLoader();

    loader.load(url, (geometry) => {

      //pawnMesh = new THREE.Mesh(geometry, pawnModelMaterial);
      pawnMesh = geometry.children[0];
      pawnMesh.material = pawnModelMaterial;
      pawnMesh.position.x = this.x;
      pawnMesh.position.y = this.y;
      pawnMesh.position.z = this.z;

      pawnMesh.scale.set(50, 50, 50);

      //mesh.castShadow = true;
      //mesh.receiveShadow = true; (?)\
      //mixer = new THREE.AnimationMixer(meshModel);

      callback(pawnMesh);

    });
  };

  this.getPawnMesh = function() {
    return pawnMesh;
  };
}