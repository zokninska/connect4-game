/*
  Represents game board object. Contains collumns array and their's pawns.
*/

function Board(boardMaterial, collumnMaterial) {

  this.collumnsArray = [];

  var boardContainer = new THREE.Object3D();

  //create collumns
  let createCollumnsArray = () => {
    var array = [];

    for (var i = 0; i < 4; i++) { //row
      var row = [];
      for (var j = 0; j < 4; j++) { //pojedyncze collumn w row

        var x = 200 + (j * 200) - 500;
        var y = 202.5;
        var z = 200 + (i * 200) - 500;

        var collumn = new Collumn(collumnMaterial, x, z);
        collumn.getCollumnMesh().position.set(x, y, z);

        row.push(collumn);
        boardContainer.add(collumn.getCollumnMesh());
      }
      array.push(row);
    }

    return array;
  };
  this.collumnsArray = createCollumnsArray();

  //create board
  var boardGeometry = new THREE.BoxGeometry(1000, 50, 1000);
  var boardMesh = new THREE.Mesh(boardGeometry, boardMaterial);
  boardMesh.castShadow = true;
  boardMesh.receiveShadow = true;

  boardContainer.add(boardMesh);

  this.getBoardContainer = function() {
    return boardContainer;
  };

  this.getBoardMesh = function() {
    return boardMesh;
  };
}