/*
  Handling user interface
*/

function UI() {

  this.init = function() {
    var player_1Div = $('<div>').text("You : ");
    var player_2Div = $('<div>').text("Opponent : ");
    var messageDiv = $('<div>');

    $(player_1Div).attr('id', 'player_1Div');
    $(player_2Div).attr('id', 'player_2Div');
    $(messageDiv).attr('id', 'messageDiv');

    $("#root").prepend(player_1Div, player_2Div, messageDiv);
  };

  this.showLogOnPanel = function() {
    var logOnPanel = $('<div>');
    var logOnInput = $('<input>');
    var logOnButton = $('<button>');

    $(logOnPanel).attr('id', 'logOnPanel');
    $(logOnInput).attr({
      'id': 'logOnInput',
      'type': 'text'
    });
    $(logOnButton).attr('id', 'logOnButton').text("Log in!");

    $(logOnPanel).append(logOnInput, logOnButton);
    $("#root").prepend(logOnPanel);

    $("#logOnButton").on("click", function() {
      net.logOnPlayer($("#logOnInput").val());
    });

  };

  this.removeLogOnPanel = function() {
    $("#logOnPanel").remove();
  };

  this.setMessage = function(message) {
    $("#messageDiv").text(message);
  };

  this.clearMessage = function() {
    $("#messageDiv").text("");
  };

  this.setPlayersNames = function(playerName_1 = "", playerName_2 = "") {
    $("#player_1Div").text("You : " + playerName_1);
    $("#player_2Div").text("Opponent : " + playerName_2);
  };

  this.resetPlayersNames = function() {
    $("#player_1Div").text("You : ");
    $("#player_2Div").text("Opponent : ");
  };

}