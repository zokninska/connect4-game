/*
  Communication Socket.io - server
*/

function Net() {
  var client = io();

  client.on("onconnect", function(data) {
    console.log("connected");
  });

  this.logOnPlayer = function(login) {
    client.emit("logOnPlayer", {
      "login": login
    });
  };

  client.on("logOnPlayer", function(data) {
    console.log("przyszlo", data);

    ui.setMessage(data.message);

    if (data.playerNumber === 1) {

      game.clientID = data.clientID;
      game.clientPawnsType = data.clientPawnsType;

      ui.setPlayersNames(data.playerName_1);
      ui.removeLogOnPanel();

    } else if (data.playerNumber === 2) {

      game.clientID = data.clientID;
      game.clientPawnsType = data.clientPawnsType;

      ui.removeLogOnPanel();
      ui.setPlayersNames(data.playerName_2, data.playerName_1);
      game.disposeBoardRotation();
      game.enableOnclickEvents();

    }

  });

  client.on("loggedOpponent", function(data) {
    console.log("przyszlo", data);

    ui.setMessage(data.message);
    ui.setPlayersNames(data.playerName_1, data.playerName_2);
    game.disableWaitingForOpponentMove();
    game.disposeBoardRotation();
    game.enableOnclickEvents();

  });

  this.onSetPawn = function(pawn) {
    client.emit("setPawn", {
      "pawn": pawn
    });
  };

  client.on("setPawn", function(data) {
    console.log("przyszlo", data);

    ui.setMessage(data.message);
    game.setWaitingForOpponentMove();

  });

  client.on("allowMove", function(data) { //tu przychodzi pawn
    console.log("przyszlo", data);

    ui.setMessage(data.message);
    game.disableWaitingForOpponentMove();
    game.setOpponentPawn(data.pawn);
    game.searchForFourPawnsInline();

  });

  this.onWin = function(pawnsArray) { //wysyła to najpierw wygrywający gracz (stawiający ostatni pionek)
    client.emit("onWin", {
      "pawnsArray": pawnsArray
    });
  };

  client.on("onWin", function(data) { //tu przychodzi pawn lub nie
    console.log("przyszlo", data);

    if (data.pawnsArray) {
      ui.setMessage(data.message);

    } else if (!data.pawnsArray) {
      ui.setMessage(data.message);
    }
    // game.disableWaitingForOpponentMove();
    // game.setOpponentPawn(data.pawn);
    //game.searchForFourPawnsInline();

  });
}