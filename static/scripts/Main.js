var game;
var ui;
var net;

$(document).ready(function() {
  game = new Game();

  ui = new UI();
  ui.init();
  ui.showLogOnPanel();

  net = new Net();
});