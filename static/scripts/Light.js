function Light() {
  var light = new THREE.PointLight(0xffd9b3, 1, 2000, 3.14);
  light.power = 20;

  this.getLight = function() {
    return light;
  };
}