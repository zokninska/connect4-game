/*
  Game class
*/

function Game() {

  //###############Public#Variables###################//

  this.clientID = null;
  this.clientPawnsType = null;

  //###############Private#Variables###################//

  //engine
  var scene, camera, renderer;
  var raycaster, mouseVector, orbitControl;

  //materials
  var groundMaterial, boardMaterial, collumnMaterial, selectedCollumnMaterial;
  var greenPawnModelMaterial, blackPawnModelMateiral, winnerPawnMatrial;

  //objects
  var axes; //na razie

  var groundGeometry, groundMesh;
  var board, boardObject;
  var light, lightFront;

  //others
  var width = window.innerWidth;
  var height = window.innerHeight;

  //game
  var isBoardRotating = true;

  var collumnsArray = [];
  var selectedCollumnObj = false;
  var lastMousemovedCollumn = false;

  var isEnabledOnclickEvent = false;
  var isWaitingForOpponentMove = true;

  //#################3D#World#Functions##################//

  function initEngine() {

    //scene
    scene = new THREE.Scene();

    //camera
    camera = new THREE.PerspectiveCamera(
      45,
      4 / 3,
      0.1,
      10000
    );
    camera.position.set(900, 1200, 900);
    camera.lookAt(scene.position);
    camera.fov = 45;
    camera.updateProjectionMatrix();

    //renderer
    renderer = new THREE.WebGLRenderer();
    renderer.setClearColor(0x000000);
    renderer.setSize(width, height);
    $("#root").append(renderer.domElement);

    //raycast
    raycaster = new THREE.Raycaster();
    mouseVector = new THREE.Vector2();

    //orbitControl
    orbitControl = new THREE.OrbitControls(camera, renderer.domElement);
    orbitControl.addEventListener('change', function() {
      renderer.render(scene, camera);
    });

  }


  function initMaterials() {

    //groundMaterial
    groundMaterial = new THREE.MeshPhongMaterial({
      side: THREE.DoubleSide,
      wireframe: false,
      specular: 0xffffff,
      shininess: 50,
    });

    //boardMaterial
    boardMaterial = new THREE.MeshPhongMaterial({
      specular: 0xffffff,
      shininess: 10,
      side: THREE.DoubleSide,
      map: new THREE.TextureLoader().load('mats/board.jpg')
    });

    //greenPawnModelMaterial
    greenPawnModelMaterial = new THREE.MeshPhongMaterial({
      map: THREE.ImageUtils.loadTexture("mats/greenPawn.jpg"),
      specular: 0x00ff00,
      shininess: 20,
    });

    //blackPawnModelMaterial
    blackPawnModelMaterial = new THREE.MeshPhongMaterial({
      map: THREE.ImageUtils.loadTexture("mats/blackPawn.jpg"),
      specular: 0xffffff,
      shininess: 50,
    });

    //winnerPawnMatrial
    winnerPawnMatrial = new THREE.MeshPhongMaterial({
      specular: 0x000000,
      shininess: 20,
    });

    //collumnMaterial
    collumnMaterial = new THREE.MeshPhongMaterial({
      side: THREE.DoubleSide,
      wireframe: false,
      color: 0x999966
    });

    //selectedCollumnMaterial
    selectedCollumnMaterial = new THREE.MeshBasicMaterial({
      side: THREE.DoubleSide,
      wireframe: false,
      color: 0xff0000
    });

  }


  function initObjects() {

    //axes
    // axes = new THREE.AxesHelper(1000); //na raize
    // scene.add(axes);

    //ground
    groundGeometry = new THREE.PlaneGeometry(4000, 4000);
    groundMesh = new THREE.Mesh(groundGeometry, groundMaterial);
    groundMesh.receiveShadow = true;
    scene.add(groundMesh);

    //board
    board = new Board(boardMaterial, collumnMaterial);
    boardObjectContainer = board.getBoardContainer();
    boardMesh = board.getBoardMesh();
    scene.add(boardObjectContainer);

    //light
    light = new Light();
    light.getLight().lookAt(scene.position);
    light.getLight().position.set(0, 900, 0);
    scene.add(light.getLight());

    // lightFront = new Light();
    // lightFront.getLight().lookAt(scene.position);
    // lightFront.getLight().lookAt(camera.position);
    // scene.add(lightFront.getLight());

  }


  let initEvents = () => {

    $(document).mousedown((event) => {
      if (isEnabledOnclickEvent) {
        if (!isWaitingForOpponentMove) {
          mouseVector.x = (event.clientX / $(window).width()) * 2 - 1;
          mouseVector.y = -(event.clientY / $(window).height()) * 2 + 1;

          raycaster.setFromCamera(mouseVector, camera);

          var intersects = raycaster.intersectObjects(scene.children, true);

          if (intersects.length > 0) {

            var mesh = intersects[0]; //is sllected mesh

            if (mesh.object.geometry.type === "CylinderGeometry") {
              selectedCollumnObj = searchForCollumnObjInArrayByMesh(mesh.object);
              createPawn(mesh.object, selectedCollumnObj);
              this.searchForFourPawnsInline();
            }
          }
        }
      }
    });

    $(document).mousemove((event) => {
      if (isEnabledOnclickEvent) {
        if (!isWaitingForOpponentMove) {
          mouseVector.x = (event.clientX / $(window).width()) * 2 - 1;
          mouseVector.y = -(event.clientY / $(window).height()) * 2 + 1;

          raycaster.setFromCamera(mouseVector, camera);

          var intersects = raycaster.intersectObjects(scene.children, true);

          if (intersects.length > 0) {

            var mesh = intersects[0]; //is s/lected mesh

            if (mesh.object.geometry.type === "CylinderGeometry") {
              if (lastMousemovedCollumn !== mesh.object) { //czy taki sam jak najechany przed chwilą
                mesh.object.material = selectedCollumnMaterial;

                if (lastMousemovedCollumn) lastMousemovedCollumn.material = collumnMaterial;
                lastMousemovedCollumn = mesh.object;
              }
            } else if (mesh.object.geometry.type !== "CylinderGeometry") {
              if (lastMousemovedCollumn !== mesh.object) lastMousemovedCollumn.material = collumnMaterial;
              lastMousemovedCollumn = false;
            }
          }
        }
      }
    });

  };


  let searchForCollumnObjInArrayByMesh = (collumnMesh) => {
    for (let row of board.collumnsArray) {
      for (let collumnObj of row) {
        if (collumnObj.getCollumnMesh() === collumnMesh) return collumnObj;
      }
    }
  };

  let searchForCollumnObjInArrayByPositions = (x, z) => {
    for (let row of board.collumnsArray) {
      for (let collumnObj of row) {
        if (collumnObj.getCollumnMesh().position.x === x && collumnObj.getCollumnMesh().position.z === z) return collumnObj;
      }
    }
  };

  let createPawn = (collumnMesh, collumnObj) => {
    var pawnModelMaterial;
    if (this.clientPawnsType === "green") pawnModelMaterial = greenPawnModelMaterial;
    if (this.clientPawnsType === "black") pawnModelMaterial = blackPawnModelMaterial;
    //console.log("pawn type", pawnModelMaterial);

    var pawnNum = collumnObj.pawns.length;
    var y = 70 + (2 * 40) * (pawnNum);

    var pawn = new Pawn(this.clientPawnsType, collumnMesh.position.x, y, collumnMesh.position.z, pawnModelMaterial);
    var addingError = collumnObj.addPawn(pawn);
    collumnObj.getCollumnMesh().material = collumnMaterial;

    if (addingError) {
      ui.setMessage(addingError);
      //remove pawn
    } else {
      pawn.loadModel("models/pawn.obj", (mesh) => {
        //console.log("pawnMesh", pawn.getPawnMesh());
        scene.add(pawn.getPawnMesh());
        net.onSetPawn(pawn);
        this.searchForFourPawnsInline();
      });
    }

  };

  let setPawnPosition = () => {
    //setInterval spadania pionka if pawn.position.y nie jes mniejsza od wyliczonej
  };


  initEngine();
  initMaterials();
  initObjects();
  initEvents();


  function render() {
    groundMesh.rotation.x = Math.PI / 2;

    if (isBoardRotating) {
      boardObjectContainer.rotation.y += 0.005;
    } else {
      boardObjectContainer.rotation.y = 0;
    }

    //lightFront.getLight().lookAt(camera.position);

    renderer.shadowMap.enabled = true;
    renderer.shadowMap.type = THREE.PCFSoftShadowMap;

    camera.lookAt(scene.position);

    requestAnimationFrame(render);
    renderer.render(scene, camera);
  }

  render();

  //###############Game#World#Functions#################//

  /* Public */

  this.setBoardRotation = function() {
    isBoardRotating = true;
  };

  this.disposeBoardRotation = function() {
    isBoardRotating = false;
  };

  this.enableOnclickEvents = function() {
    isEnabledOnclickEvent = true;
  };

  this.disableWaitingForOpponentMove = function() {
    isWaitingForOpponentMove = false;
  };

  this.setWaitingForOpponentMove = function() {
    isWaitingForOpponentMove = true;
  };

  this.setOpponentPawn = (pawn) => {
    var pawnModelMaterial;
    if (pawn.type === "green") pawnModelMaterial = greenPawnModelMaterial;
    if (pawn.type === "black") pawnModelMaterial = blackPawnModelMaterial;

    var x = pawn.x;
    var z = pawn.z;
    var collumnOfPawn = searchForCollumnObjInArrayByPositions(x, z);

    var pawnNum = collumnOfPawn.pawns.length;
    var y = 70 + (2 * 40) * (pawnNum);

    var opponentPawn = new Pawn(pawn.type, collumnOfPawn.x, y, collumnOfPawn.z, pawnModelMaterial);
    var addingError = collumnOfPawn.addPawn(opponentPawn);

    if (addingError) {
      ui.setMessage(addingError);
      //remove opponentPawn
    } else {
      opponentPawn.loadModel("models/pawn.obj", (mesh) => {
        //console.log("pawnMesh", opponentPawn.getPawnMesh());
        scene.add(opponentPawn.getPawnMesh());
        this.searchForFourPawnsInline();
      });
    }

  };

  this.searchForFourPawnsInline = function() {

    //pion
    var pawnType = false;
    var count = 0;
    var winningPawns = [];
    var finishedSearching = false;

    function isPawnUndefined(collumn, i) {
      return collumn.pawns[i] == undefined;
    }

    for (let row of board.collumnsArray) { //dla każdego row
      for (var j = 0; j < 4; j++) { //poziom z górę (dla każdego poziomu)
        for (var i = 0; i < row.length; i++) { //dla każdego elementu row

          // if (row.every((collumn) => {
          //     console.log(collumn.pawns[j] !== undefined);
          //     return collumn.pawns[j] !== undefined;
          //   })) {
          //   //console.log("exist pawn");
          // } else {
          //   //console.log("not  pawn");
          // }
          //
          // for (let collumn of row) {
          //
          // }

          // if (row[i].pawns[j] !== undefined && row[i + 1].pawns[j] !== undefined) { //jeżeli 2 pierwsze pionki istnieją
          //   if (row[i].pawns[j].type === row[i + 1].pawns[j].type) { //assing pawn type
          //     console.log("type");
          //     pawnType = row[i].pawns[j].type;
          //     count = 2;
          //     console.log("set count", count);
          //     winningPawns.push(row[i].pawns[j], row[i + 1].pawns[j]);
          //   }
          // } else {
          //   //console.log("not type!");
          // }
          // //
          //
          // if (pawnType && count < 4) { //jeżeli 2 pierwsze pionki są takiego sameg type
          //   console.log("IIII", i, i + 1);
          //   if (row[i + 1] !== undefined && row[i + 1].pawns[j] !== undefined) { //jeżeli 2 pierwsze pionki istnieją
          //     if (pawnType === row[i + 1].pawns[j].type) {
          //       console.log("added pawn");
          //       winningPawns.push(row[i].pawns[j]);
          //       count++;
          //       console.log("count", count);
          //     }
          //   }
          //
          // } else if (count === 4) {
          //   console.log("break out of all loops");
          //   finishedSearching = true;
          //   break; //out of all fors
          // } else { //jeżeli 2 pierwsze pionki nie są takiego sameg type
          //   console.log("break out of row, not the same first two");
          //   winningPawns = [];
          //   continue; //stop checking this row on this level
          // }

          if (i === 0) { //for first two pawns
            if (row[i].pawns[j] !== undefined && row[i + 1].pawns[j] !== undefined) { //jeżeli 2 pierwsze pionki istnieją
              if (row[i].pawns[j].type === row[i + 1].pawns[j].type) { //assing pawn type
                pawnType = row[i].pawns[j].type;
                count = 2;
                console.log("set count", count);
                winningPawns.push(row[i].pawns[j], row[i + 1].pawns[j]);
              }
            } else {
              //console.log("not type!");
            }
          } else { //forom third (i ==1)

            if (pawnType && count < 4 && i + 1 < 4) { //jeżeli 2 pierwsze pionki są takiego sameg type
              console.log(count, "IIII", i, i + 1);
              console.log("spe", row[i + 1] !== undefined, row[i + 1].pawns[j] !== undefined);
              if (row[i + 1] !== undefined && row[i + 1].pawns[j] !== undefined) { //jeżeli 2 pierwsze pionki istnieją
                if (pawnType === row[i + 1].pawns[j].type) {
                  console.log("added pawn");
                  winningPawns.push(row[i + 1].pawns[j]);
                  count++;
                  console.log("count", count);
                }
              }

            } else if (count === 4) {
              console.log("break out of all loops");
              finishedSearching = true;
              break; //out of all fors
            } else { //jeżeli 2 pierwsze pionki nie są takiego sameg type
              console.log("break out of row, not the same first two");
              winningPawns = [];
              continue; //stop checking this row on this level
            }

          }

        }
      }
    }

    if (finishedSearching) {
      for (var pawn of winningPawns) {
        pawn.getPawnMesh().material = winnerPawnMatrial;
      }
      net.onWin(winningPawns);
    }

  };
}