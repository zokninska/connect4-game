/*
  Represents game collumn. Contains pawns pushed on a collumn
*/

function Collumn(collumnMaterial, x, z) {
  this.x = x;
  this.z = z;
  this.pawns = [];

  //create collumn
  var collumnGeometry = new THREE.CylinderGeometry(10, 10, 405, 20); //znienić wymiary śrenica 10
  var collumnMesh = new THREE.Mesh(collumnGeometry, collumnMaterial);
  collumnMesh.castShadow = true;
  //boardMesh.receiveShadow = true;

  this.getCollumnMesh = function() {
    return collumnMesh;
  };


  //operacje na pionkach (usuwa tylko z talbicy : pomyśle nad usuwaniem ze sceny, to akutrat pewnie napiszemy w board : zobaczymy)
  this.addPawn = function(pawn) {
    //sprawdz talbice, policz pozycje  i new Pawn(x, y) //od policz pozycji (może tu ew animacja spadnięcia pionka)
    //zappenduj pawn do this.pawns
    if (this.pawns.length < 4) {
      this.pawns.push(pawn);
      return false;
    } else {
      return "Collumn already full!";
    }
  };
}