var http = require("http");
var fs = require("fs");
var qs = require("querystring");
var Datastore = require('nedb');
var socketio = require("socket.io");

var clientsIDArray = [];
var winner = false;

var server = http.createServer(function(req, res) {

  switch (req.method) {

    case "GET":
      console.log(req.method);
      if (req.url === "/") {

        fs.readFile("static/index.html", function(error, data) {

          if (error) {
            res.writeHead(404, {
              'Content-Type': 'text/html'
            });
            res.write("<h1>404 - brak takiej strony</h1>");
            res.end();
          } else {
            res.writeHead(200, {
              'Content-Type': 'text/html'
            });
            res.write(data);
            res.end();
          }

        });

      } else if (req.url === "/styles/style.css") {

        fs.readFile("static/styles/style.css", function(error, data) {

          if (error) {
            res.writeHead(404, {
              'Content-Type': 'text/html'
            });
            res.write("<h1>404 - brak takiej strony</h1>");
            res.end();
          } else {
            res.writeHead(200, {
              'Content-Type': 'text/css'
            });
            res.write(data);
            res.end();
          }

        });

      } else if (req.url === "/scripts/Light.js") {

        fs.readFile("static/scripts/Light.js", function(error, data) {

          if (error) {
            res.writeHead(404, {
              'Content-Type': 'text/html'
            });
            res.write("<h1>404 - brak takiej strony</h1>");
            res.end();
          } else {
            res.writeHead(200, {
              'Content-Type': 'application/javascript'
            });
            res.write(data);
            res.end();
          }

        });

      } else if (req.url === "/scripts/Pawn.js") {

        fs.readFile("static/scripts/Pawn.js", function(error, data) {

          if (error) {
            res.writeHead(404, {
              'Content-Type': 'text/html'
            });
            res.write("<h1>404 - brak takiej strony</h1>");
            res.end();
          } else {
            res.writeHead(200, {
              'Content-Type': 'application/javascript'
            });
            res.write(data);
            res.end();
          }

        });

      } else if (req.url === "/scripts/Collumn.js") {

        fs.readFile("static/scripts/Collumn.js", function(error, data) {

          if (error) {
            res.writeHead(404, {
              'Content-Type': 'text/html'
            });
            res.write("<h1>404 - brak takiej strony</h1>");
            res.end();
          } else {
            res.writeHead(200, {
              'Content-Type': 'application/javascript'
            });
            res.write(data);
            res.end();
          }

        });

      } else if (req.url === "/scripts/Board.js") {

        fs.readFile("static/scripts/Board.js", function(error, data) {

          if (error) {
            res.writeHead(404, {
              'Content-Type': 'text/html'
            });
            res.write("<h1>404 - brak takiej strony</h1>");
            res.end();
          } else {
            res.writeHead(200, {
              'Content-Type': 'application/javascript'
            });
            res.write(data);
            res.end();
          }

        });

      } else if (req.url === "/scripts/UI.js") {

        fs.readFile("static/scripts/UI.js", function(error, data) {

          if (error) {
            res.writeHead(404, {
              'Content-Type': 'text/html'
            });
            res.write("<h1>404 - brak takiej strony</h1>");
            res.end();
          } else {
            res.writeHead(200, {
              'Content-Type': 'application/javascript'
            });
            res.write(data);
            res.end();
          }

        });

      } else if (req.url === "/scripts/Net.js") {

        fs.readFile("static/scripts/Net.js", function(error, data) {

          if (error) {
            res.writeHead(404, {
              'Content-Type': 'text/html'
            });
            res.write("<h1>404 - brak takiej strony</h1>");
            res.end();
          } else {
            res.writeHead(200, {
              'Content-Type': 'application/javascript'
            });
            res.write(data);
            res.end();
          }

        });

      } else if (req.url === "/scripts/Game.js") {

        fs.readFile("static/scripts/Game.js", function(error, data) {

          if (error) {
            res.writeHead(404, {
              'Content-Type': 'text/html'
            });
            res.write("<h1>404 - brak takiej strony</h1>");
            res.end();
          } else {
            res.writeHead(200, {
              'Content-Type': 'application/javascript'
            });
            res.write(data);
            res.end();
          }

        });

      } else if (req.url === "/scripts/Main.js") {

        fs.readFile("static/scripts/Main.js", function(error, data) {

          if (error) {
            res.writeHead(404, {
              'Content-Type': 'text/html'
            });
            res.write("<h1>404 - brak takiej strony</h1>");
            res.end();
          } else {
            res.writeHead(200, {
              'Content-Type': 'application/javascript'
            });
            res.write(data);
            res.end();
          }

        });

      } else if (req.url === "/libs/jquery-3.3.1.js") {

        fs.readFile("static/libs/jquery-3.3.1.js", function(error, data) {

          if (error) {
            res.writeHead(404, {
              'Content-Type': 'text/html'
            });
            res.write("<h1>404 - brak takiej strony</h1>");
            res.end();
          } else {
            res.writeHead(200, {
              'Content-Type': 'application/javascript'
            });
            res.write(data);
            res.end();
          }

        });

      } else if (req.url === "/libs/OrbitControls.js") {

        fs.readFile("static/libs/OrbitControls.js", function(error, data) {

          if (error) {
            res.writeHead(404, {
              'Content-Type': 'text/html'
            });
            res.write("<h1>404 - brak takiej strony</h1>");
            res.end();
          } else {
            res.writeHead(200, {
              'Content-Type': 'application/javascript'
            });
            res.write(data);
            res.end();
          }

        });

      } else if (req.url === "/libs/three.js") {

        fs.readFile("static/libs/three.js", function(error, data) {

          if (error) {
            res.writeHead(404, {
              'Content-Type': 'text/html'
            });
            res.write("<h1>404 - brak takiej strony</h1>");
            res.end();
          } else {
            res.writeHead(200, {
              'Content-Type': 'application/javascript'
            });
            res.write(data);
            res.end();
          }

        });

      } else if (req.url === "/libs/OBJLoader.js") {

        fs.readFile("static/libs/OBJLoader.js", function(error, data) {

          if (error) {
            res.writeHead(404, {
              'Content-Type': 'text/html'
            });
            res.write("<h1>404 - brak takiej strony</h1>");
            res.end();
          } else {
            res.writeHead(200, {
              'Content-Type': 'application/javascript'
            });
            res.write(data);
            res.end();
          }

        });

      } else if (req.url === "/mats/board.jpg") {

        fs.readFile("static/mats/board.jpg", function(error, data) {

          if (error) {
            res.writeHead(404, {
              'Content-Type': 'text/html'
            });
            res.write("<h1>404 - brak takiej strony</h1>");
            res.end();
          } else {
            res.writeHead(200, {
              'Content-Type': 'image/jpeg'
            });
            res.write(data);
            res.end();
          }

        });

      } else if (req.url === "/mats/blackPawn.jpg") {

        fs.readFile("static/mats/blackPawn.jpg", function(error, data) {

          if (error) {
            res.writeHead(404, {
              'Content-Type': 'text/html'
            });
            res.write("<h1>404 - brak takiej strony</h1>");
            res.end();
          } else {
            res.writeHead(200, {
              'Content-Type': 'image/jpeg'
            });
            res.write(data);
            res.end();
          }

        });

      } else if (req.url === "/mats/greenPawn.jpg") {

        fs.readFile("static/mats/greenPawn.jpg", function(error, data) {

          if (error) {
            res.writeHead(404, {
              'Content-Type': 'text/html'
            });
            res.write("<h1>404 - brak takiej strony</h1>");
            res.end();
          } else {
            res.writeHead(200, {
              'Content-Type': 'image/jpeg'
            });
            res.write(data);
            res.end();
          }

        });

      } else if (req.url === "/models/pawn.obj") {

        fs.readFile("static/models/pawn.obj", function(error, data) {

          if (error) {
            res.writeHead(404, {
              'Content-Type': 'text/html'
            });
            res.write("<h1>404 - brak takiej strony</h1>");
            res.end();
          } else {
            res.writeHead(200, {
              'Content-Type': 'application/javascript'
            });
            res.write(data);
            res.end();
          }

        });

      } else {

        res.writeHead(404, {
          'Content-Type': 'text/html'
        });
        res.write("<h1>404 - brak takiej strony</h1>");
        res.end();

      }
      break;

    case "POST":
      console.log(req.method);
      break;
  }

});

server.listen(3000, function() {
  console.log("serwer startuje na porcie 3000");
});

//socket.io
var io = socketio.listen(server); // server -> server nodejs

io.sockets.on("connection", function(client) {
  console.log("klient się podłączył " + client.id);

  client.emit("onconnect", {});

  client.on("disconnect", function() {
    console.log("klient się rozłącza");
    //rozłącz drugiego też
  });

  client.on("logOnPlayer", function(data) {
    collection.count({}, function(err, playersAmount) {
      var doc;

      if (playersAmount === 0) { //if it's first player

        doc = {
          "login": data.login,
          "playerNumber": playersAmount + 1,
          "points": 0
        };
        insertToDb(doc);

        clientsIDArray.push(client.id);

        var IDindex = clientsIDArray.indexOf(client.id);
        var clientPawnsType = IDindex === 0 ? "green" : "black";

        collection.findOne({ //send his data from database
          playerNumber: 1
        }, function(err, doc) {
          var emitData = {
            "playerNumber": doc.playerNumber,
            "message": "You logged as first player! Waiting for second player...",
            "playerName_1": doc.login,
            "clentID": clientsIDArray[IDindex],
            "clientPawnsType": clientPawnsType
          };

          console.log("emit", emitData);
          io.sockets.to(clientsIDArray[0]).emit("logOnPlayer", emitData);
        });

      } else if (playersAmount === 1) { //if it's second player

        collection.count({
          "login": data.login
        }, function(err, loginsAmount) {
          console.log("dokumentów jest: ", loginsAmount);

          if (loginsAmount === 0) { //if his login is not duplicated

            doc = {
              "login": data.login,
              "playerNumber": playersAmount + 1,
              "points": 0
            };
            insertToDb(doc);

            clientsIDArray.push(client.id);

            var IDindex = clientsIDArray.indexOf(client.id);
            var clientPawnsType = IDindex === 0 ? "green" : "black";

            collection.find({}, function(err, doc) { //send two players data from database
              var reverse = false;
              console.log("texst", doc[0].login, doc[1].login);
              if (doc[0].playerNumber === 2) reverse = true;

              var playerName_1 = reverse ? doc[1].login : doc[0].login;
              var playerName_2 = reverse ? doc[0].login : doc[1].login;

              var emitData = {
                "playerNumber": playersAmount + 1,
                "message": "You logged as second player! Waiting for opponents move...",
                "playerName_1": playerName_1,
                "playerName_2": playerName_2,
                "clentID": clientsIDArray[IDindex],
                "clientPawnsType": clientPawnsType
              };

              console.log("emit", emitData);
              io.sockets.to(clientsIDArray[1]).emit("logOnPlayer", emitData);

              io.sockets.to(clientsIDArray[0]).emit("loggedOpponent", {
                "message": "Your time to make a move!",
                "playerName_1": playerName_1,
                "playerName_2": playerName_2,
              });
            });

          } else { //if sec player name is duplicated

            var emitData = {
              message: "This login is aleready used, try again!"
            };
            console.log("emit", emitData);
            io.sockets.to(client.id).emit("logOnPlayer", emitData);

          }
        });

      } else { //if there are already 2 players selected

        var emitData = {
          message: "Two players already selected!"
        };
        console.log("emit", emitData);
        io.sockets.to(client.id).emit("logOnPlayer", emitData);

      }

    });

  });

  client.on("setPawn", function(data) {
    var IDindex = clientsIDArray.indexOf(client.id);
    var opponentID = IDindex === 0 ? clientsIDArray[1] : clientsIDArray[0];

    io.sockets.to(client.id).emit("setPawn", {
      message: "Waiting for opponents move..."
    });

    console.log(data.pawn);
    io.sockets.to(opponentID).emit("allowMove", {
      message: "Your time to make a move!",
      pawn: data.pawn
    });
  });

  client.on("onWin", function(data) {
    collection.remove({}, {
      multi: true
    }, function(err, numRemoved) {});

    var IDindex, opponentID;

    if (!winner) {
      winner = client.id;

      IDindex = clientsIDArray.indexOf(client.id);
      opponentID = IDindex === 0 ? clientsIDArray[1] : clientsIDArray[0];
    }

    io.sockets.to(winner).emit("onWin", {
      message: "You have won! End of game!",
      pawnsArray: false
    });

    console.log(data.pawnsArray);
    io.sockets.to(opponentID).emit("onWin", {
      message: "You have failed! End of game!",
      pawnsArray: data.pawnsArray
    });
  });

});

//nedb
//create new collection
var collection = new Datastore({
  filename: 'gameBase.db',
  autoload: true
});

function insertToDb(doc) {
  collection.insert(doc, function(err, newDoc) {
    //console.log(newDoc);
  });
}