Connect 4

1. Instrukcja instalacji  i uruchomienia gry

-żeby grać w grę, potrzebne są 2 osoby - jedna uruchamia serwer, 2 jest klientem
-Osoba będąca serwerem :
	-musi ściągnąć z internetu i zainstalować program Node.js (jeśli jest już zainstalowany, można pominąć ten krok)
	-Następnie uruchomić program cmd.exe (jeśli używasz systenu operacyjnego Windows) lub terminal (jeśli używasz Linux'a) i przejdź do katalogu zawierającego grę (poleceniem 'cd ścieżka_do_katalogu')
	-Wpisać polecenie node serwer
	-Później przejść do przeglądarki (najlepiej Mozilla Firefox) i wpisać swój_adres_IP:3000 (można sprawdzić w cmd.exe poleceniem ipconfig lub terminalu poleceniem ifconfig)
	 UWAGA: oba komputery muszą być w tej samej sieci
-Osoba będąca klientem musi wpisać w polu wyszukiwania przeglądarki (najlepiej Mozilla Firefox) adres_IP_serwera:3000

2. Jak grać

-Obie osoby wpisują swoje nicki (muszą być różne)
-Po zalogowaniu się obu osób, pierwsza osoba wybiera słupek kliknięciem, na którym umieszcza swoją kulkę, następnie drugi gracz robi to samo itd.
-Wygrywa gracz, który w jakimkolwiek rzędzie lub pionie będzie miał 4 swoje pionki
-Po wygranej, należy wyłączyć, a następnie podążać za instrukcjami z pkt.1, od podpunktu z włĄczeniem serwera włącznie
-Jeśli wystapił błąd, należy postąpić podobnie, jak w przypadku wygranej któregoś z graczy, jednakże przed uruchomieniem serwera,
należy usunąć zawartość pliku gamebase.db, znajdującym się w katalogu głównym gry
